#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 08:06:21 2021

@author: Cyrile Delestre
"""

import pytest
from ernie.utils import (list_of_dict_2_dict_of_list as l2d, 
                         dict_of_list_2_list_of_dict as d2l)

def test_l2d():
    a = [dict(a=1, b='a'), dict(a=2, b='b')]
    assert l2d(a, type_trans=list) == dict(a=[1, 2], b=['a', 'b'])

def test_d2l():
    a = dict(a=[1, 2], b=['a', 'b'])
    assert d2l(a) == [dict(a=1, b='a'), dict(a=2, b='b')]

