#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  2 08:16:59 2021

@author: Cyrile Delestre
"""

import pytest
import numpy as np

from ernie.database_engine import (DocIndexer, WordIndexer,
                                   set_query_from_metadata as sqlite_set,
                                   get_query_from_metadata as sqlite_get)

def test_docindexer():
    list_docs = [DocIndexer(1, 1, 1), DocIndexer(0, 0, 0),
                 DocIndexer(2, None, 1), DocIndexer(2, 0, 1)]
    res_sort = [DocIndexer(0, 0, 0), DocIndexer(1, 1, 1),
                DocIndexer(2, None, 1), DocIndexer(2, 0, 1)]
    assert sorted(list_docs) == res_sort

def test_worindexer():
    list_words = [WordIndexer(2), WordIndexer(0), WordIndexer(1)]
    res_sort = [WordIndexer(0), WordIndexer(1), WordIndexer(2)]
    assert sorted(list_words) == res_sort

def test_sqlite_set():
    query = sqlite_set(
        data=dict(
            idx=[DocIndexer(1, 1, 1), DocIndexer(0, 0, 0)],
            text=['Il faut sauver le soldat Ryan', 'Le jour le plus long'],
            vec=[np.random.rand(2, 100), np.random.rand(2, 100)]
        ),
        idx_name='idx',
        table_name='doc_base'
    )
    assert query == (
        "INSERT INTO "
        "doc_base(doc_idx, paragraph_idx, sentence_idx, text, vec) "
        "VALUES (?, ?, ?, ?, ?);"
    )

def test_sqlite_get_doc():
    query = sqlite_get(
        fields=['text', 'vec'],
        indexer=[DocIndexer(1, 1, 1), DocIndexer(0, 0, 0),
                 DocIndexer(42, None, None)],
        table_name='doc_base'
    )
    assert query == (
        "SELECT text, vec FROM doc_base WHERE "
        "(doc_idx=1 AND paragraph_idx=1 AND sentence_idx=1) OR "
        "(doc_idx=0 AND paragraph_idx=0 AND sentence_idx=0) OR "
        "(doc_idx=42 AND paragraph_idx IS NULL AND sentence_idx IS NULL);"
    )

def test_sqlite_get_word():
    query = sqlite_get(
        fields=['word', 'vec'],
        indexer=[WordIndexer(1), WordIndexer(0), WordIndexer(42)],
        table_name='word_base'
    )
    assert query == (
        "SELECT word, vec FROM word_base WHERE word_idx IN (1, 0, 42);"
    )
