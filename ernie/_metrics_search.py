#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Mesure de similarité

Created on Fri Dec 25 13:15:02 2020

@author: Cyrile Delestre
"""

from typing import Union, overload
from os import path
from pickle import dump, load

import numpy as np
from sklearn.preprocessing import normalize
from sklearn.metrics.pairwise import (cosine_similarity, euclidean_distances,
                                      manhattan_distances)
from sklearn.neighbors import BallTree, DistanceMetric
from x250.data import PandasSQL

class Similarity:
    r"""
    Méthodes de similarité normé permettant de déterminer la proximité des 
    documents par :
        cosine :
            Similarité par cosinus.
        l2 :
            Similarité par distance euclidienne normalisé.
        l1 :
            Similarité par distance de Manhattan.
    
    Authors
    -------
    Cyrile Delestre - cyrile.delestre@arkea.com
    """
    cosine = lambda X, Y: cosine_similarity(X, Y, dense_output=True)
    
    l2 = lambda X, Y: 1-euclidean_distances(
            X=normalize(X, norm='l2'),
            Y=normalize(Y, norm='l2'),
            squared=False
        )
    
    l1 = lambda X, Y: 1-manhattan_distances(
        X=normalize(X, norm='l1'),
        Y=normalize(Y, norm='l1'),
        sum_over_features=True
    )

class SpeedSearchNeighbors(BallTree):
    r"""
    Classe surchagé de `BallTree`_ permettant d'exclure très rapidement les 
    document qui sont éloigné d'un vecteur dans la réprésentation latente de 
    la base de donnée textuelle. Permet ainsi d'accélerer grandement la phase 
    de recherche et calculant de similarité que sur les documents voisins.
    
    Parameters
    ----------
    X : np.ndarray (nb_docs x features)
        Représentation dans l'espace latente des documents
    save_path : str
        Chemin de sauvegarde du moteur.
    leaf_size : int (=40)
        Nombre de documents présent minimum présent d'une une hyper-sphère.
    metric : Union[str, DistanceMetric] (='euclidean')
        Métrique de proximité entre les document.
    
    Notes
    -----
    La phase d'apprentissage de la base peut prendre un peu de temps si elle 
    est conséquente (compter 5 min. pour 1 millons de documents en moyenne). 
    Si un nouveau document est ajouter à la base, il faudra créer un nouvel 
    SpeedSearchNeighbors. Donc si le but est de faire une base qui s'allimente 
    en temps réelle SpeedSearchNeighbors n'est pas forcément la bonne solution 
    même si l'approche accelère grandement la recherche.
    
    Warning
    -------
    Attention à la métrique utilisée. Si la mesure de similarité utilisée est 
    de type cosinus, il est important d'utiliser :
        metric = 'cosine'
    Car cette mesure ne prend en compte que la colinéarité des vecteurs et non 
    la norme de ces derniers (deux vecteurs peuvent être loin au sens des 
    distances euclidienne ou de Manhattan mais cependant colinéaire au sens 
    de similarité de cosinus).
    
    Authors
    -------
    Cyrile Delestre - cyrile.delestre@arkea.com
    
    .. BallTree: https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.BallTree.html
    """
    def __init__(self,
                 X: np.ndarray,
                 save_path: str,
                 leaf_size: int=40,
                 metric: Union[str, DistanceMetric]='euclidean',
                 **kwargs):
        if not path.exists(path.dirname(save_path)):
            raise AttributeError(
                f"Le chemin {path.dirname(save_path)} n'existe pas."
            )
        self.save_path = save_path
        # Pour le moment ne fonctionne pas...
        X = X.copy().astype(np.float32)
        super().__init__(
            X,
            leaf_size=leaf_size,
            metric=metric,
            **kwargs
        )
        dump(self, open(self.save_path, 'bw'))
    
    def _vectors_from_query(self, query: np.ndarray, k: Union[int, float]):
        if isinstance(k, int):
            return self.query(
                query,
                k=k,
                return_distance=False,
                sort_results=False
            )[0]
        elif isinstance(k, float):
            return self.query_radius(
                query,
                r=k,
                return_distance=False,
                sort_results=False
            )[0]
        else:
            raise AttributeError(
                f"k doit être de type int ou float et non de type {type(k)}."
            )
    
    @overload
    def get_vectors_from_query(self,
                               query: np.ndarray,
                               doc_vec: np.ndarray,
                               k: Union[int, float]=0.1):
        r"""
        Permet de sortir les vecteurs des documents dans l'espace latent les 
        plus proche du vecteur de la requête.
        
        Parameters
        ----------
        query : np.ndarray (1 x features)
            Vecteur de la requête dans l'espace latent.
        doc_vec : np.ndarray (nb_docs x features)
            Matrice des documents embedded.
        k : Union[int, float] (=0.1)
            Deux comportement en fonction du type :
                - int : retourne les k documents les plus proche du vecteurs 
                    requête ;
                - float : retourne tous les documents compris dans le rayon 
                    k autour du vecteur requête.
        
        Returns
        -------
        vec : np.ndarray (neighbors_doc x features)
            Matrice des représentations latentes des documents les plus proche 
            du vecteur requête.
        """
        return doc_vec[self._vectors_from_query(query, k)]
    
    @overload
    def get_vectors_from_query(self,
                               query: np.ndarray,
                               doc_db: PandasSQL,
                               idx_field: str,
                               vec_field: str,
                               table_name: str,
                               k: Union[int, float]=0.1):
        r"""
        Permet de sortir les vecteurs des documents dans l'espace latent les 
        plus proche du vecteur de la requête pour des documents non stocké en 
        RAM mais dans une base relationnelle de type SQL.
        query : np.ndarray (1 x features)
            Vecteur de la requête dans l'espace latent.
        doc_db : PandasSQL
            Base de donnée PandasSQL (package x250).
        idx_field : str
            Nom de la colonne des indices
        vec_field : str
            Nom de la colonne des vecteurs documents
        table_name : str
            Nom de la table
        k : Union[int, float] (=0.1)
            Deux comportement en fonction du type :
                - int : retourne les k documents les plus proche du vecteurs 
                    requête ;
                - float : retourne tous les documents compris dans le rayon 
                    k autour du vecteur requête.
        
        Returns
        -------
        vec : np.ndarray (neighbors_doc x features)
            Matrice des représentations latentes des documents les plus proche 
            du vecteur requête.
        """
        idx = self._vectors_from_query(query, k)
        with doc_db as db:
            vec = np.stack(
                db(
                    f"SELECT {vec_field} FROM {table_name} WHERE "
                    f"{idx_field} IN ({', '.join(idx)});"
                ),
                axis=0
            )
        return vec
    
    def get_vectors_from_query(self,
                               query: np.ndarray,
                               k: Union[int, float]=0.1):
        r"""
        Permet de sortie les indices des documents les plus proche du vecteur 
        requête dans l'espace latent de la base.
        
        Parameters
        ----------
        query : np.ndarray (1 x features)
            Vecteur de la requête dans l'espace latent.
        k : Union[int, float] (=0.1)
            Deux comportement en fonction du type :
                - int : retourne les k documents les plus proche du vecteurs 
                    requête ;
                - float : retourne tous les documents compris dans le rayon 
                    k autour du vecteur requête.
        
        Returns
        -------
        idx : np.ndarray (neighbors_doc,)
            Indices des documents proches dans l'espace latent du vecteur de 
            requête.
        """
        return self._vectors_from_query(query, k)
    
    @classmethod
    def load(cls, save_path: str):
        r"""
        Méthode classe permettant de chager le moteur de recherche BallTree.
        
        Parameters
        ----------
        save_path : str
            Chemin où le moteur SpeedSearchNeighbors a été seuvegardé.
        """
        if path.exists(save_path):
            raise AttributeError(
                f"Le modèle SpeedSearchNeighbors au chemin {save_path} "
                "n'existe pas."
            )
        return load(open(save_path, 'br'))
