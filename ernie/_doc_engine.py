#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Classes et fonctions d'indexation.

Created on Sun Dec 27 12:36:31 2020

@author: Cyrile Delestre
"""

import sqlite3
from os import path
from typing import List, Dict, Any, Union

from x250.data import index_sql
from x250.utils import list_of_dict_2_dict_of_list as l2d

# class DocumentEngine:
#     def __init__(self, path_of_table: str, **kargs_sql):
#         self._table_name = 'docs'
#         self.base = SQLite(path_of_trable, **kargs_sql)
#         if path.exists(path_of_table):
#             with self.base:
#                 self.base.create_table(
#                     table_name=self._table_name
#                 )
#             index_sql(
#                 path_sql=path_of_trable,
#                 table_name=self._table_name,
#                 list_keys=Indexer.list(),
#                 index_name='doc_index',
#                 unique=True
#             )
    
#     def __getitem__(self, idx: Indexer):
#         return self.base[idx]
    
#     def __setitem__(self, idx: Indexer, item: Dict[str, Any]):
#         if isinstance(items, list):
#             items = l2d(items)
#         with self.base as db:
#             'insert'
    
#     def setitmes(self, items: List[Dict[str, Any]]):
#         'a'
    
    # def __len__(self):
    #     with self.base as db:
    #         nb_rows = db.(f"COUNT(*) FROM {self._table_name};").values[0]
    #     return nb_rows
