#!/usr/bin/env python3
# -*- coding: utf-8 -*

# Version du package ERNIE
__version__ = '0.1'

from ._metrics_search import Similarity, SpeedSearchNeighbors

__all__ = (
    'Similarity',
    'SpeedSearchNeighbors'
)
