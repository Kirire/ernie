#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Classes d'indexation des documents et des mots

Created on Wed Dec 30 10:29:17 2020

@author: Cyrile Delestre
"""

from dataclasses import dataclass
from typing import Optional, List, Tuple, Dict, Callable

@dataclass
class DocIndexer:
    r"""
    Objet d'indexation pour les documents.
    
    Parameters
    ----------
    Un document est définit par :
        - doc_idx : int
            représentant le document ;
        - paragraph_idx : Optional[int] (=None)
            représentant la praragraphe dans le document ;
        - sentence_idx : Optional[int] (=None)
            représentant la phrase du paragraphe (ou document).
    L'indexer doit être unique dans la base.
    
    Exemples
    --------
    >>> from ernie.database_engine import DocIndexer
    >>> 
    >>> list_docs = [DocIndexer(1, 1, 1), DocIndexer(0, 0, 0),
    >>>              DocIndexer(2, None, 1), DocIndexer(2, 0, 1),
    >>>              DocIndexer(1, 1, 1)]
    >>> sorted(list_docs)
    [DocIndexer(doc_idx=0, paragraph_idx=0, sentence_idx=0),
     DocIndexer(doc_idx=1, paragraph_idx=1, sentence_idx=1),
     DocIndexer(doc_idx=1, paragraph_idx=1, sentence_idx=1),
     DocIndexer(doc_idx=2, paragraph_idx=None, sentence_idx=1),
     DocIndexer(doc_idx=2, paragraph_idx=0, sentence_idx=1)]
    >>> set(list_docs)
    {DocIndexer(doc_idx=0, paragraph_idx=0, sentence_idx=0),
     DocIndexer(doc_idx=1, paragraph_idx=1, sentence_idx=1),
     DocIndexer(doc_idx=2, paragraph_idx=0, sentence_idx=1),
     DocIndexer(doc_idx=2, paragraph_idx=None, sentence_idx=1)}
    
    Notes
    -----
    Les indexations des paragraphes et phrases sont optionnelles, par contre 
    l'indexation du document n'est pas optionnelle.
    
    See Also
    --------
    WordIndexer
    """
    doc_idx: int
    paragraph_idx: Optional[int]=None
    sentence_idx: Optional[int]=None
    
    def __post_init__(self):
        if self.doc_idx is None:
            raise AttributeError(
                "L'attribut doc_idx ne peut pas être de type NoneType."
            )
    
    def __hash__(self):
        return hash(self.get_flat_index())
    
    def __eq__(self, other) -> bool:
        return self.get_flat_index() == other.get_flat_index()
    
    def __ne__(self, other) -> bool:
        return self.get_flat_index() != other.get_flat_index()
    
    def __lt__(self, other) -> bool:
        return self._compare(other, lambda x, y: x < y)
    
    def __le__(self, other) -> bool:
        return self._compare(other, lambda x, y: x <= y)
    
    def __gt__(self, other) -> bool:
        return self._compare(other, lambda x, y: x > y)
    
    def __ge__(self, other) -> bool:
        return self._compare(other, lambda x, y: x >= y)
    
    def _compare(self, other, comparator_op: Callable):
        if ((self.sentence_idx is None and self.paragraph_idx is None) or 
            (other.sentence_idx is None and other.paragraph_idx is None)):
            return comparator_op(self.doc_idx, other.doc_idx)
        elif self.sentence_idx is None or other.sentence_idx is None:
            return comparator_op(
                (self.doc_idx, self.paragraph_idx),
                (other.doc_idx, other.paragraph_idx)
            )
        elif self.paragraph_idx is None or other.paragraph_idx is None:
            return comparator_op(
                (self.doc_idx, self.sentence_idx),
                (other.doc_idx, other.sentence_idx)
            )
        else:
            return comparator_op(
                self.get_flat_index(),
                other.get_flat_index()
            )
    
    def get_index(self) -> Tuple[Optional[int]]:
        r"""
        Méthode permettant de récupérer l'indexation au formet tuple.
        
        Returns
        -------
        res : Tuple
            tuple(doc_idx, paragraph_idx, sentence_idx)
        """
        return self.doc_idx, self.paragraph_idx, self.sentence_idx
    
    def get_dict(self) -> Dict[str, Optional[int]]:
        r"""
        Méthode permettant de redéscendre l'indexeur au format dictionaire.
        
        Returns
        -------
        res : Dict[str, Optional[int]]
            Dict(doc_idx, paragraph_idx, sentence_idx)
        """
        return self.__dict__
    
    def get_attr(self) -> List[str]:
        r"""
        Méthode retournant les noms des attribue de l'indexeur.
        
        Returns
        -------
        res : List[str]
            ['doc_idx', 'paragraph_idx', 'sentence_idx']
        """
        return list(self.__dict__.keys())
    
    @classmethod
    def get_keys(cls) -> List[str]:
        r"""
        Méthode classe permettant d'obtenir les attributs qui définit un 
        document.
        
        Returns
        -------
        res : List[str]
            ['doc_idx', 'paragraph_idx', 'sentence_idx']
        """
        a = cls(0)
        return a.get_attr()

@dataclass
class WordIndexer:
    r"""
    Objet d'indexation des mots. Le mot est définit par un indice seule.
    
    Parameters
    ----------
    word_idx : int
        Indice du mot.
    
    Exemples
    --------
    >>> from ernie.database_engine import WordIndexer
    >>> 
    >>> list_words = [WordIndexer(1), WordIndexer(0), WordIndexer(2),
    >>>               WordIndexer(1)]
    >>> sorted(list_words)
    [WordIndexer(word_idx=0), WordIndexer(word_idx=1), WordIndexer(word_idx=1),
     WordIndexer(word_idx=2)]
    >>> set(list_words)
    {WordIndexer(word_idx=0), WordIndexer(word_idx=1),
     WordIndexer(word_idx=2)}
    
    See Also
    --------
    DocIndexer
    """
    word_idx: int
    
    def __post_init__(self):
        if self.word_idx is None:
            raise AttributeError(
                "L'attribut word_idx ne peut pas être de type NoneType."
            )
    
    def __hash__(self):
        return hash(self.get_index())
    
    def __eq__(self, other) -> bool:
        return self.word_idx == other.word_idx
    
    def __ne__(self, other) -> bool:
        return self.word_idx != other.word_idx
    
    def __lt__(self, other) -> bool:
        return self.word_idx < other.word_idx
    
    def __le__(self, other) -> bool:
        return self.word_idx <= other.word_idx
    
    def __gt__(self, other) -> bool:
        return self.word_idx > other.word_idx
    
    def __ge__(self, other) -> bool:
        return self.word_idx >= other.word_idx
    
    def get_index(self) -> (int):
        r"""
        Méthode permettant de récupérer l'indexation au formet tuple.
        
        Returns
        -------
        res : Tuple
            tuple(word_idx)
        """
        return (self.word_idx)
    
    def get_dict(self) -> Dict[str, int]:
        r"""
        Méthode permettant de redéscendre l'indexeur au format dictionaire.
        
        Returns
        -------
        res : Dict[str, int]
            Dict(word_idx)
        """
        return self.__dict__
    
    def get_attr(self) -> List[str]:
        r"""
        Méthode retournant les noms des attribue de l'indexeur.
        
        Returns
        -------
        res : List[str]
            ['word_idx']
        """
        return list(self.__dict__.keys())
    
    @classmethod
    def get_keys(cls) -> List[str]:
        r"""
        Méthode classe permettant d'obtenir les attributs qui définit un 
        document.
        
        Returns
        -------
        res : List[str]
            ['word_idx']
        """
        a = cls(0)
        return a.get_attr()
