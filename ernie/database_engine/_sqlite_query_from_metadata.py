#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Transformation des metadatas vers le langage pour les bases de données.

Created on Thu Dec 31 10:57:36 2020

@author: Cyrile Delestre
"""

from typing import List, Dict, Any, Union

from ernie.database_engine import DocIndexer, WordIndexer

DictOfList = Dict[List[Union[DocIndexer, WordIndexer]], List[Any]]
ListOfDict = List[Dict[Union[DocIndexer, WordIndexer], Any]]

def create_table_query(fields: Dict[str, str],
                       indexer: Union[DocIndexer, WordIndexer],
                       table_name: str,):
    r"""
    Construction des queries SQLite permettant de créer la base de documents 
    ou de mots + création de l'indexation.
    
    Parameters
    ----------
    fields : Dict[str, str]
        Dictionnaire correspondant au nom des champs de la base + le type 
        SQLite du champs.
    indexer : Union[DocIndexer, WordIndexer]
        Instantiateur de l'indexeur de document ou de mot.
    table_name : str
        Nom de la table SQLite.
    """
    query_create_table = (
        f"CREATE TABLE {table_name} "
        f"({', '.join(f'{kk} {vv}' for kk, vv in fields.items())});"
    )
    query_create_index = (
        "CREATE UNIQUE INDEX "
        f"idx_{'docs' if isinstance(indexer(0), DocIndexer) else 'words'} "
        f"ON {table_name}({', '.join(indexer.get_keys())});"
    )
    return query_create_table, query_create_index

def get_query_from_metadata(fields: List[str],
                            indexer: List[Union[DocIndexer, WordIndexer]],
                            table_name: str):
    r"""
    Construction de la query SQLite qui permet de récuéprer la donné en 
    fonction d'un indexer (DocIndexer ou WordIndexer).
    
    Parameters
    ----------
    fields : List[str]
        Liste des champs que l'on souhaiter retourner.
    indexer : List[Union[DocIndexer, WordIndexer]]
        Liste d'indexer DocIndexer ou WordIndexer correspondant aux documents 
        ou aux mots qu'on souhaite retourner.
    table_name : str
        Nom de la table SQLite.
    """
    return (
        f"SELECT {', '.join(fields)} FROM {table_name} WHERE "
        f"{sql_parse_indexer(indexer)};"
    )

def set_query_from_metadata(data: Union[DictOfList, ListOfDict],
                            idx_name: str,
                            table_name: str):
    r"""
    Construction de la query permettant d'insérer des données dans une base 
    de documents ou mots indéxées par un WordIndexer ou DocIndexer.
    
    Parameters
    ----------
    data : Union[DictOfList, ListOfDict]
        Liste de dictionnaire ou dictionnaire de liste où chaque clef du 
        dictionnaire représente un indexer de type WordIndexer ou DocIndexer 
        suivit des champs à intégrer à la base.
    idx_name : str
        Nom de la clef contenant l'indexer.
    table_name : str
        Nom de la table SQLite.
    """
    if isinstance(data, dict):
        data_col = list(data.keys())
        data_col.remove(idx_name)
        all_col = data[idx_name][0].get_keys() + data_col
    elif isinstance(data, list):
        data_col = list(data[0].keys())
        data_col.remove(idx_name)
        all_col = data[0][idx_name].get_keys() + data_col
    else:
        raise AttributeError(
            f"data doit être de type {DictOfList} ou {ListOfDict} et "
            f"non de type {type(data)}."
        )
    return (
        f"INSERT INTO {table_name}({', '.join(all_col)}) "
        f"VALUES ({', '.join(['?']*len(all_col))});"
    )

def sql_parse_indexer(indexer: List[Union[DocIndexer, WordIndexer]]):
    r"""
    Fonction privé permettant de parser les indexers de type DocIndexer et 
    WordIndexer pour être compatible avec SQLite.
    """
    if isinstance(indexer[0], DocIndexer):
        item = [
            ' AND '.join(
                f'{kk}={vv}' if vv is not None else f'{kk} IS NULL'
                for kk, vv in ind.get_dict().items()
            )
            for ind in indexer
        ]
        return f"({') OR ('.join(item)})"
    elif isinstance(indexer[0], WordIndexer):
        idx_name = indexer[0].get_keys()[0]
        return (
            f"{idx_name} IN "
            f"({', '.join([str(ii.get_index()) for ii in indexer])})"
        )
    else:
        raise AttributeError(
            "indexer doit être une liste de DocIndexer ou une liste de "
            f"WordIndexer et non de type {type(indexer)} de "
            f"{type(indexer[0])}."
        )