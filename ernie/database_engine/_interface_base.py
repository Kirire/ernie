#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Interface de l'ensemble des bases de données

Created on Wed Dec 30 09:14:41 2020

@author: cyriledelestre
"""

import abc
import sqlite3
from dataclasses import dataclass, field
from os import path
from typing import List, Dict, Any, Union
from pickle import dumps, loads
from sqlite3 import Binary

from ernie.database_engine import DocIndexer, WordIndexer
from ernie.utils import list_of_dict_2_dict_of_list as l2d
from ernie.utils.abtract_class import SuperclassDocstring

DictOfList = Dict[List[Union[DocIndexer, WordIndexer]], List[Any]]
ListOfDict = List[Dict[Union[DocIndexer, WordIndexer], Any]]

@dataclass
class DataBaseInterface(metaclass=SuperclassDocstring):
    r"""
    Classe interface où pour chaque méthode obligatoire fournie suffisament
    d'argument pour gérer une grande partie des type de base de donnée SQL et 
    NoSQL.
    """
    path_base: str
    base_type: Union[DocIndexer, WordIndexer]
    kwargs_base: Dict[str, Any]
    
    @abc.abstractmethod
    def create_database(self,
                        table_name: str,
                        fields: Dict[str, str]) -> None:
        r"""
        Méthode permettant de créer une table ou un indexer.
        
        Parameters
        ----------
        table_name : str
            Nom de la table ou de l'indexer.
        fields : Dict[str, str]
            Nom des champs avec leurs types.
        """
        raise NotImplementedError(
            "L'interface doit être munie de la méthode create_database "
            "permettant de créer une table (SQL) ou un indexer (NoSQL)."
        )
    
    @abc.abstractmethod
    def get_data(self,
                 fields: List[str],
                 indexer: List[Union[DocIndexer, WordIndexer]],
                 table_name: str) -> Dict[str, Any]:
        r"""
        Méthode permettant de récupérer de la donnée à partir d'une requête.
        
        Parameters
        ----------
        fields : List[str]
            Nom des champs en sortie de la requête.
        indexer: List[Union[DocIndexer, WordIndexer]]
            List d'index d'extraction.
        table_name : str
            Nom de la table ou de l'indexer.
        """
        raise NotImplementedError(
            "L'interface doit être munie de la méthode read_data permettant "
            "de récupérer une donnée à partir d'une query."
        )
    
    @abc.abstractmethod
    def set_data(self,
                 data: Union[DictOfList, ListOfDict],
                 idx_name: str,
                 table_name: str) -> None:
        r"""
        Méthode permettant d'intégrer un élément à la base.
        
        Parameters
        ----------
        data: Union[DictOfList, ListOfDict]
            Liste de dictionnaire ou de dictionnaire de liste ayant un 
            identifiant unique et la donnée.
        idx_name : str
            Nom de la clef contenant l'indexer.
        table_name : str
            Nom de la table ou de l'indexer.
        """
        raise NotImplementedError(
            "L'interface doit être munie de la méthode insert_data "
            "permettant d'insérer une ou plusieurs entrée(s) dans la base."
        )
        

@dataclass
class SQLite:
    r"""
    Classe permettant de faciliter l'utilisation de SQLite via le context 
    manager de Python.
    """
    path_sql: str
    kwargs_sql: Dict[str, Any] = field(default_factory=dict)
    
    def __enter__(self):
        r"""
        Entrée du contexte.
        """
        self.conn = sqlite3.connect(self.path_sql, **self.kwargs_sql)
        return self.conn
    
    def __exit__(self, error_type, error_value, error_traceback):
        r"""
        Sortie du contexte.
        """
        self.conn.commit()
        self.conn.close()
        # del important sinon objet non Picklelisable
        del self.conn

@dataclass
class SQLiteIterface(DataBaseInterface):
    path_base: str
    base_type: Union[DocIndexer, WordIndexer]
    fields: Dict[str, str] = field(default_factory=dict)
    kwargs_base: Dict[str, Any] = field(default_factory=dict)
    
    def __post_init__(self):
        if not isinstance(self.base_type(0), (DocIndexer, WordIndexer)):
            raise AttributeError(
                "L'arrtibut base_type doit être la l'instancieur DocIndexer "
                "ou WordIndexer."
            )
        global create_table_query, get_query_from_metadata
        global set_query_from_metadata
        from ernie.database_engine import (create_table_query,
                                           get_query_from_metadata,
                                           set_query_from_metadata)
        self.sql_db = SQLite(self.path_base, self.kwargs_base)
    
    def _transform_blob(self,
                        data: Union[DictOfList, ListOfDict]) -> DictOfList:
        r"""
        Méthode privée permettant de transformer les blobs d'assurer le 
        passage en DictOfList.
        
        Parameters
        ----------
        data : Union[DictOfList, ListOfDict]
            
        """
        blob_fields = [kk for kk, vv in self.fields.items() if vv == 'BLOB']
        if isinstance(data, list):
            data = l2d(data, type_trans=list)
        elif isinstance(data, dict):
            data = data.copy()
        else:
            raise AttributeError(
                f"data doit être de type {DictOfList} ou {ListOfDict} et "
                f"non de type {type(data)}."
            )
        blob = set(data).intersection(blob_fields)
        for bb in blob:
            data[bb] = [Binary(dumps(ii)) for ii in data[bb]]
        return data
    
    def create_database(self,
                        table_name: str) -> None:
        query_create_table, query_create_index = create_table_query(
            fields=self.fields,
            indexer=self.base_type,
            table_name=table_name
        )
        with self.sql_db as conn:
            cur = conn.cursor()
            cur.execute(query_create_table)
            cur.execute(query_create_index)
    
    def get_data(self,
                 fields: List[str],
                 indexer: List[Union[DocIndexer, WordIndexer]],
                 table_name: str) -> Dict[str, Any]:
        blob_fields = [kk for kk, vv in self.fields.items() if vv == 'BLOB']
        query = get_query_from_metadata(fields, indexer, table_name)
        with self.sql_db as conn:
            cur = conn.cursor()
            cur.execute(query)
            data = cur.fetchall()
        data = dict(zip(fields, zip(*data)))
        for kk in data:
            if kk in blob_fields:
                data[kk] = [loads(ii) for ii in data[kk]]
            else:
                data[kk] = list(data[kk])
        return data
    
    def set_data(self,
                 data: Union[DictOfList, ListOfDict],
                 idx_name: str,
                 table_name: str) -> None:
        query, data_struct = set_query_from_metadata(
            data=data,
            idx_name=idx_name,
            table_name=table_name
        )
        data = self._transform_blob(data)
        data_struct = list(zip(*data.values()))
        with self.sql_db as conn:
            cur = conn.cursor()
            cur.executemany(query, data_struct)
        return None
