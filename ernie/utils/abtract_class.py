#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Classes de gestions des classes abstraites.

Created on Sun Jan  3 13:20:45 2021

@author: Cyrile Delestre
"""

import abc

class SuperclassDocstring(abc.ABCMeta):
    r"""
    Méta classe permettant de faire hériter la documention d'une calsse 
    abstraite à la classe concrète.
    """
    def __new__(mcls, classname, bases, cls_dict):
        cls = abc.ABCMeta.__new__(mcls, classname, bases, cls_dict)
        mro = cls.__mro__[1:]
        for name, member in cls_dict.items():
            if not getattr(member, '__doc__'):
                for base in mro:
                    try:
                        member.__doc__ = getattr(base, name).__doc__
                        break
                    except AttributeError:
                        pass
        return cls
