#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ._transpose import (list_of_dict_2_dict_of_list,
                         dict_of_list_2_list_of_dict)

__all__ = (
    'list_of_dict_2_dict_of_list',
    'dict_of_list_2_list_of_dict'
)
