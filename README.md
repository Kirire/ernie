# ERNIE

<!-- <img src=`docs/source/_static/ernie.png` width=`200`/> -->
![](docs/source/_static/ernie.png)

- Free software: Apache Software License 2.0

# Overview

**ERNIE** pour **Engine for Research from Natural Interpretation Embbeding** est un moteur de recherche sur des bases de documents textuelles par sémantique. Le moteur s'appuis sur sentence-transformers et Hugging Face Transformers. Le moteur peux fonctionner aussi bien sur des documents anglais que d'autre l'angue du moment que le modèle de réprésentation de text universelle dans un espace latent est compatible avec Hugging Face Transformers. 

**ERNIE** est compatible avec `Python >= 3.8`.

## Release Notes
### 0.1

  * Création du projet

