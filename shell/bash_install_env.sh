#!/bin/bash
# bash shell d'installation de l'environnement

# update ou installation des libraires utiles pour le projet
conda env update --file "../environment.yml" --prune --prefix ../venv

# Test si Dash fait partie de l'environnement si oui
# est que le script project_name_app.py n'exite pas
# création de ce dernier à la racine du projet.

# Test si dash fait partie de l'environnement
source activate ../venv
DASH_PRES=`conda list | grep dash | wc -l`

py_cut=$(cat <<-EOM
import sys

pwd = sys.argv[1]
sep = sys.argv[2]
pos = eval(sys.argv[3])
print(pwd.split(sep)[pos])
EOM
)

# Si présent crée un scipt python project_name_app.py
# contenant le stater du dash si pas présent
if [ $DASH_PRES -gt 0 ]
then
    PROJ_NAME=$(python -c "$py_cut" `pwd` "/" "-2")
    NAME_EXE_DASH=$PROJ_NAME"_dash_exe.py"
    APP_PRES=`ls .. | grep $NAME_EXE_DASH | wc -l`
    if [ $APP_PRES -eq 0 ]
    then
        cat <<EOF > ../$PROJ_NAME/visualization/dash_app.py
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Création de l'objet app de Dash.
Ici doit être fait les callback, etc.

Created on Thu Feb  6 11:48:35 2020

@author: Cyrile Delestre
"""
from dash import Dash

app = Dash(name=__name__)
app.css.config.serve_locally = True
app.scripts.config.serve_locally = False
app.config['suppress_callback_exceptions'] = False
app.title = 'Titre du Dash'

EOF
        cat <<EOF > ../$NAME_EXE_DASH
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script de lancement du serveur Dash.
Il faudra modifer le port du serveur.

Created on Thu Feb  6 11:48:35 2020

@author: Cyrile Delestre
"""
from $PROJ_NAME.visualization.dash_app import *

if __name__=='__main__':
    # Par défaut port à 8080 à changer si port déjà pris
    app.run_server(port=8080, debug=True, host='0.0.0.0')

EOF
    fi
fi

