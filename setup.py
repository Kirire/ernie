import os
try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

    def find_packages(where='.'):
        # os.walk -> list[(dirname, list[subdirs], list[files])]
        return [folder.replace("/", ".").lstrip(".")
                for (folder, _, fils) in os.walk(where)
                if "__init__.py" in fils]

import builtins

builtins.__SETUP__ = True

import ernie

with open("README.md") as readme_file:
    readme = readme_file.read()

requirements = [
    "joblib>=0.17.0",
    "numpy>=1.19.0",
    "sklearn>=0.22",
    "pandas>=1.1.4",
    "intel-openmp!=2019.5",
    "tqdm>=4.52.0"
]

setup(
    name="ERNIE",
    version=ernie.__version__,
    platforms=["any"],
    packages=["ernie"]+[f"ernie.{ii}" for ii in find_packages("ernie")],
    url="https://gitlab.com/Kirire/ernie",
    python_requires=">=3.5",
    include_package_data = True,
    license="Apache Software License 2.0",
    author="Cyrile Delestre",
    author_email="cyrile.delestre@arkea.com",
    description="Package de moteur de recherche sémantique.",
    install_requires=requirements,
    setup_requires=["pytest-runner"],
    test_suite="tests",
    tests_require=["pytest"],
    long_description=readme,
    zip_safe=False
)

